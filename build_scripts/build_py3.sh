#!/usr/bin/env bash

apt-get -y update

apt-get -y install python3-pip
apt-get -y install python3-dev python3-setuptools
apt-get -y install git
apt-get -y install supervisor

pip3 install --upgrade pip
pip3 install -r requirements.txt
pip3 freeze
