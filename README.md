## What is `new-guid`? 
`new-guid` is a simple CLI command which returns a new, pseudo-random, GUID / UUID when called.

## Installation
`pip install new-guid`

*OR*

`sudo snap install new-guid`*

**Only valid on systems which support Ubuntu snaps.*

## Use
```bash
cwandrews@kmaster:~$ new-guid 
1C778973-ACDB-4182-8BC2-C909348C899F
cwandrews@kmaster:~$ 
``` 