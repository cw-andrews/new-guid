import pytest

from core.guid import newguid


def test_newguid_returns_str():
    assert isinstance(newguid(), str)


def test_newguid_default_upper():
    working_guid = newguid()
    assert working_guid == working_guid.upper()


def test_newguid_override_lower():
    working_guid = newguid(upper=False)
    assert working_guid == working_guid.lower()


if __name__ == '__main__':
    pytest.main(['-v'])
